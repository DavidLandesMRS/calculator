Please read this!

Before opening a new issue, make sure to search for keywords in the issues

filtered by the "New Feature" label to ensure your issue is not addressed

in another issue.

Please remove this notice if you're confident your issue isn't a duplicate.

------

### Description

(Include problem, use cases, benefits, and/or goals)

### Proposal

How should we tackle this new feature?  Include screenshots, mockups, or any

other details to communicate how this new feature should work.

### Documentation

(What should be documented in the project wiki?)

/label ~"Type::New Feature"
