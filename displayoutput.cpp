#include "displayoutput.h"
#include <QDebug>

Calculator::Calculator(QObject *parent)
    : QObject{parent}, m_displayText("0"), m_fontSize(40) {

}

//  Getter for output display text
QString Calculator::displayText() const {
    return m_displayText;
}

//  Getter for output text font size
int Calculator::fontSize() const {
    return m_fontSize;
}

//  Setter for output display text
void Calculator::setDisplayText(const QString &newDisplayText) {

    if (m_displayText == newDisplayText)
        return;
    m_displayText = newDisplayText;
    emit displayTextChanged();

}

/*  Method to append the value of a pressed button to the m_displayText field...
 *  the button value won't be appended if 0 is pressed and the output is already "0"
 *  or if the current output is already too long. After the value is appended,
 *  the adjustFontSize() method changes font size accordingly.
*/
void Calculator::push(const QString &newDisplayText) {

    if (newDisplayText == "0" && m_displayText.at(0) == "0") {
        return;
    }
    if (m_fontSize <= 25) {
        return;
    }
    if (m_displayText.at(0) == "0") { //Get rid of the placeholder 0
        m_displayText = "";
    }
    m_displayText += newDisplayText;
    qDebug() << newDisplayText << " added to output";

    adjustFontSize();

    emit displayTextChanged();

}

//  Method to reset the display to 0 and reset output font size to 40
void Calculator::clearDisplay() {

    m_displayText = "0";
    qDebug() << "display cleared";
    emit displayTextChanged();
    m_fontSize = 40;
    qDebug() << "font size reset to 40";
    emit fontSizeChanged();

}

/*  Method to change font size as the output length grows...
 *  it finds the maximum font size that will still show the whole
 *  display, with a hard minimum size of 25. This method is called
 *  every time a value is appended, so if the font size doesn't need
 *  changed the method just keeps it at 40.
*/
void Calculator::adjustFontSize() {

    const double FONT_40_IMPLICIT_LENGTH = 30.09375; //Size of one character, font size 40, in the output
    const double LENGTH_CHANGE_1_POINT = 0.7; //Estimate for how much implicit length changes as font size increments/decrements
    const int MINIMUM_FONT_SIZE = 25;
    double outputImplicitLength;

    if (m_displayText.length() * FONT_40_IMPLICIT_LENGTH > 326) {
        for (int fontSize = 40; fontSize >= MINIMUM_FONT_SIZE; fontSize--) {
            outputImplicitLength = m_displayText.length() * (FONT_40_IMPLICIT_LENGTH - ((40 - fontSize) * LENGTH_CHANGE_1_POINT));
            if (outputImplicitLength < 326) {
                qDebug() << "new font size: " << fontSize;
                m_fontSize = fontSize;
                emit fontSizeChanged();
                return;
            }
        }
        m_fontSize = 25;
        emit fontSizeChanged();
    } else {
        m_fontSize = 40;
        emit fontSizeChanged();
    }

}
