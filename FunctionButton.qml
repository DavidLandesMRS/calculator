import QtQuick 2.15

//  Sets styles for all buttons that perform a function

Rectangle {
    property string buttonText: ""
    signal clicked()

    id: rectId
    width: 80
    height: 50
    radius: 5
    color: "#333333"

    Text {
        color: "#ffffff"
        font.pointSize: 15
        text: buttonText
        anchors.centerIn: parent
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            rectId.clicked()
        }
    }

}

