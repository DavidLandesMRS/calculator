import QtQuick 2.15
import QtQuick.Window 2.15
import bennett.calculator 1.0

/*
   All buttons are contained within a Grid element, which is contained
   in a Column under the output Text element. Button click handlers
   call C++ methods to update output text and output font size.
   If the window is resized, the calculator stays the same size but is
   horizontally centered.

   UI Issues:
   1. Repeating/irrational decimals need to be truncated after a given
      number of decimal places...this will probably be implemented
      outside the QML but does need fixed
*/

Window {
    id: rootWindow
    width: 346
    height: 400
    visible: true
    title: qsTr("My Calculator")
    color: "#202020"

    Calculator {
        id: outputCppObject
    }

    Rectangle { //Border for calculator if window is resized (purely aesthetic)
        id: outsideBorderRect
        border.color: "#454545"
        border.width: 2
        anchors.horizontalCenter: parent.horizontalCenter
        width: 346
        height: 400
        color: "#202020"

        Column { //Contains output text, grid of buttons
            id: displayParent
            spacing: 20
            padding: 10

            Rectangle { //Background color for output text
                id: outputContainerRect
                height: 102
                width: 326
                color: "#202020"

                Text { //Displayed output text
                    id: outputText
                    text: outputCppObject.displayText
                    font.pointSize: outputCppObject.fontSize
                    font.bold: true
                    color: "white"
                    anchors.right: outputContainerRect.right
                    anchors.bottom: outputContainerRect.bottom
                }
            }

            Grid {
                id: buttonContainerGrid
                columnSpacing: 2
                rowSpacing: 2
                columns: 4

                //Each button has a custom MouseArea because they have to
                //pass the correct argument to the C++ methods

                //First row of buttons
                FunctionButton {
                    id: buttonReset
                    buttonText: "AC"
                }

                FunctionButton {
                    id: buttonClear
                    buttonText: "C"

                    onClicked: {
                        outputCppObject.clearDisplay()
                        outputText.text = outputCppObject.displayText
                        outputText.font.pointSize = outputCppObject.fontSize
                    }
                }

                FunctionButton {
                    id: buttonDelete
                    buttonText: "<<"
                }

                FunctionButton {
                    id: buttonDivide
                    buttonText: "/"

                    onClicked: {

                    }
                }

                //Second row of buttons
                NumberButton {
                    id: button7
                    buttonText: "7"

                    onClicked: {
                        outputCppObject.push("7")
                        outputText.text = outputCppObject.displayText
                        outputText.font.pointSize = outputCppObject.fontSize
                    }
                }

                NumberButton {
                    id: button8
                    buttonText: "8"

                    onClicked: {
                        outputCppObject.push("8")
                        outputText.text = outputCppObject.displayText
                        outputText.font.pointSize = outputCppObject.fontSize
                    }
                }

                NumberButton {
                    id: button9
                    buttonText: "9"

                    onClicked: {
                        outputCppObject.push("9")
                        outputText.text = outputCppObject.displayText
                        outputText.font.pointSize = outputCppObject.fontSize
                    }
                }

                FunctionButton {
                    id: buttonMultiply
                    buttonText: "x"

                    onClicked: {

                    }
                }

                //Third row of buttons
                NumberButton {
                    id: button4
                    buttonText: "4"

                    onClicked: {
                        outputCppObject.push("4")
                        outputText.text = outputCppObject.displayText
                        outputText.font.pointSize = outputCppObject.fontSize
                    }
                }

                NumberButton {
                    id: button5
                    buttonText: "5"

                    onClicked: {
                        outputCppObject.push("5")
                        outputText.text = outputCppObject.displayText
                        outputText.font.pointSize = outputCppObject.fontSize
                    }
                }

                NumberButton {
                    id: button6
                    buttonText: "6"

                    onClicked: {
                        outputCppObject.push("6")
                        outputText.text = outputCppObject.displayText
                        outputText.font.pointSize = outputCppObject.fontSize
                    }
                }

                FunctionButton {
                    id: buttonSubtract
                    buttonText: "--"

                    onClicked: {

                    }
                }

                //Fourth row of buttons
                NumberButton {
                    id: button1
                    buttonText: "1"

                    onClicked: {
                        outputCppObject.push("1")
                        outputText.text = outputCppObject.displayText
                        outputText.font.pointSize = outputCppObject.fontSize
                    }
                }

                NumberButton {
                    id: button2
                    buttonText: "2"

                    onClicked: {
                        outputCppObject.push("2")
                        outputText.text = outputCppObject.displayText
                        outputText.font.pointSize = outputCppObject.fontSize
                    }
                }

                NumberButton {
                    id: button3
                    buttonText: "3"

                    onClicked: {
                        outputCppObject.push("3")
                        outputText.text = outputCppObject.displayText
                        outputText.font.pointSize = outputCppObject.fontSize
                    }
                }

                FunctionButton {
                    id: buttonAdd
                    buttonText: "+"

                    onClicked: {

                    }
                }

                //Fifth row of buttons
                NumberButton {
                    id: buttonNegative
                    buttonText: "+/-"
                }

                NumberButton {
                    id: button0
                    buttonText: "0"

                    onClicked: {
                        outputCppObject.push("0")
                        outputText.text = outputCppObject.displayText
                        outputText.font.pointSize = outputCppObject.fontSize
                    }
                }

                NumberButton {
                    id: buttonDecimal
                    buttonText: "."
                }

                Rectangle { //Custom equals button because of different color
                    id: buttonEquals
                    width: 80
                    height: 50
                    radius: 5
                    color: "#cabee7"

                    Text {
                        color: "#000000"
                        font.pointSize: 15
                        text: "="
                        anchors.centerIn: parent
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {

                        }
                    }
                }
            }
        }
    }
}


