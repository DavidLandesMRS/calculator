#ifndef DISPLAYOUTPUT_H
#define DISPLAYOUTPUT_H

#include <QObject>

class Calculator : public QObject
{
    Q_OBJECT

/*  QML properties for the text sent to the calculator's output
 *  and the font size of the text
*/
    Q_PROPERTY(QString displayText READ displayText WRITE setDisplayText NOTIFY displayTextChanged)
    Q_PROPERTY(int fontSize READ fontSize NOTIFY fontSizeChanged)

public:
    explicit Calculator(QObject *parent = nullptr);

//  Getters and setters
    QString displayText() const;
    int fontSize() const;
    void setDisplayText(const QString &newDisplayText);

//  Custom methods
    Q_INVOKABLE void push(const QString &newDisplayText);
    Q_INVOKABLE void clearDisplay();
    void adjustFontSize();

signals:
    void displayTextChanged();
    void fontSizeChanged();

private:
    QString m_displayText;
    int m_fontSize;
};

#endif // DISPLAYOUTPUT_H
